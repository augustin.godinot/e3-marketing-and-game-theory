# Viral marketing and game theory

## Installation

1. Install [conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html)
2. Create the *Conda* environment from the specified file: `conda env create -f environment.yml`
3. Activate the environment `conda activate game-theory`

## Generating the figures

The three figures in the report are generated with the following commands :
1. `python main.py article-figure-1`
2. `python main.py article-figure-2`
3. `python main.py social-welfare`
