from typing import Optional

import numpy as np
from scipy import sparse
from scipy.sparse.linalg import expm
from constants import REVENUE_PER_CONSUMER, ADVERTISING_EFFICIENCY, TOTAL_BUDGET


def waterfilling(levels: np.ndarray, total_volume: float, widths: Optional[np.ndarray] = None) -> np.ndarray:
    """Perform waterfilling power allocation

    It finds an allocation such that
    x[n] = weight[n] * (allocation[n] - level[n])^+
    \sum_n x[n] = total

    Parameters
    ----------
    levels : np.ndarray (n, )
        The floor levels
    total_volume : float
        The total volume of water available
    widths : np.ndarray (n, )
        The widths of each level

    Returns
    -------
    np.ndarray (n, )
        The allocated water height to each level
    """

    n = levels.shape[0]
    if widths is None:
        widths = np.ones(n, dtype=float)

    permutation = np.argsort(levels)
    levels_sorted = levels[permutation]
    widths_sorted = widths[permutation]

    depth = np.zeros_like(levels, dtype=float)
    water_level = levels_sorted[0]
    more_water = True
    i = 0

    while i < n - 1 and more_water:
        remaining_water = total_volume - np.sum(depth * widths_sorted)
        level_difference = levels_sorted[i+1] - levels_sorted[i]

        # If there is not enought water to fill up to the next level totally
        if remaining_water < level_difference * np.sum(widths_sorted[:i+1]):
            # Slit the remaining water evenly among this level and the previous
            # ones
            achievable_difference = remaining_water \
                / np.sum(widths_sorted[:i+1])
            depth[:i+1] += achievable_difference

            water_level += achievable_difference
            more_water = False

        # If we can fill totally up to the next level
        else:
            # Just fill them
            depth[:i+1] += level_difference
            water_level += level_difference

        i += 1

    # If we have some power left
    remaining_water = (total_volume - np.sum(depth * widths_sorted))
    if remaining_water > 0:
        depth += remaining_water / np.sum(widths_sorted)

    # Come back to the original ordering
    # better : compute the inverse permutation
    depth[permutation] = depth.copy()

    return depth, water_level


def agent_influence_power(laplacian: sparse.csc_matrix, T: float) -> np.ndarray:
    """Return the Agent Influence Power (AIP) of each node in the graph defined
    by the given Laplacian

    Parameters
    ----------
    laplacian : np.csc_matrix (n_agents, n_agents)
        The Laplacian of the graph representing the influence relations between
        agents.
    T : float
        The time horizon during which we let the agents opinon evolve before
        computing their AIP.

    Returns
    -------
    np.ndarray (n_agents,)
        The AIP of each agent
    """
    return np.ones(laplacian.shape[0]) @ expm(- laplacian * T)


def post_campaign_opinion(initial_opinion: np.ndarray, allocations: np.ndarray) -> np.ndarray:
    """Computes the opinion of each agent just after the campaign (at t = 0+)

    Parameters
    ----------
    initial_opinion : np.ndarray (n_agents, )
        The opinions of the agents towards firm 1 before the campaign. The
        opinions of the agents towards the firm 2 are deduced with the equation
        x_{n,i} = 1 - x_{n, -i}
    allocations : np.ndarray (n_agents, 2)
        The budget allocated by each of the two players to each agent

    Returns
    -------
    np.ndarray (n_agents,)
        The opinions of all the agents towards the firm 1
    """
    return (initial_opinion + allocations[:, 0]) \
        / (1 + np.sum(allocations, axis=1))


def net_revenue(initial_opinion: np.ndarray, aip: np.ndarray, allocations: np.ndarray) -> np.ndarray:
    """Computes the utility functions of the two firms

    Parameters
    ----------
    initial_opinion : np.ndarray (n_agents, )
        The opinions of the agents towards firm 1 before the campaign. The
        opinions of the agents towards the firm 2 are deduced with the equation
        x_{n,i} = 1 - x_{n, -i}
    aip : np.ndarray (n_agents, )
        The Agent Influence Power
    allocations : np.ndarray (n_agents, 2)
        The budget allocated by each of the two players to each agent

    Returns
    -------
    np.ndarray (2, )
        The utility of each firm
    """
    opinion = post_campaign_opinion(initial_opinion, allocations)

    return (
        REVENUE_PER_CONSUMER * aip @ opinion
        - ADVERTISING_EFFICIENCY * np.sum(allocations[:, 0]),
        REVENUE_PER_CONSUMER * aip @ (1 - opinion)
        - ADVERTISING_EFFICIENCY * np.sum(allocations[:, 1])
    )


def best_response(initial_opinions: np.ndarray, allocations: np.ndarray, agent_influence_power: np.ndarray, total_budget: float):
    """Implement the best game response dynamics

    Parameters
    ----------
    initial_opinions : np.ndarray (n_agents,)
        The initial opinions of the agents towards the firm 1, after
        allocations
    allocations : np.ndarray (n_agents, 2)
        The current allocations for each agent by each firm
    agent_influence_power : np.ndarray (n_agents, )
        The Agent Influence Power of each agent
    total_budget : float
        The total budget allocated by each firm
    """

    n_agents = agent_influence_power.shape[0]

    # Rename variables for reading purposes. We use the same notations as in the
    # article "Marketing resource allocation in duopolies over social networks
    # (2018 Varma et al.)"
    aip = agent_influence_power
    x_0 = np.zeros((n_agents, 2))
    x_0[:, 0] = initial_opinions
    x_0[:, 1] = 1 - initial_opinions
    a = allocations
    d = aip.reshape((n_agents, 1)) * (
        np.flip(x_0, axis=1) + np.flip(a, axis=1)
    )
    alpha = np.zeros_like(d, dtype=float)

    # Waterfilling allocation for player 1
    levels = (1 + a[:, 1]) / np.sqrt(REVENUE_PER_CONSUMER * d[:, 0])
    widths = np.sqrt(REVENUE_PER_CONSUMER * d[:, 0])

    allocation, _ = waterfilling(
        levels,
        total_budget,
        widths
    )
    # We do not compare to b_i since we assume b_i == B_i
    alpha[:, 0] = widths * allocation

    # Waterfilling allocation for player 2
    levels = (1 + a[:, 0]) / np.sqrt(REVENUE_PER_CONSUMER * d[:, 1])
    widths = np.sqrt(REVENUE_PER_CONSUMER * d[:, 1])

    allocation, _ = waterfilling(
        levels,
        total_budget,
        widths
    )
    # We do not compare to b_i since we assume b_i == B_i
    alpha[:, 1] = widths * allocation

    return alpha


def learn_best_response(initial_opinions: np.ndarray, agent_influence_power: np.ndarray, total_budget: float, n_iterations: Optional[int] = 10) -> tuple[np.ndarray, np.ndarray]:
    """Learn the Nash Equilibrium by iterating the firms' best reponse functions

    Parameters
    ----------
    initial_opinions : np.ndarray (n_agents,)
        The initial opinions of the agents towards firm 1(before any ad
        campaign)
    agent_influence_power : np.ndarray (n_agents, )
        The Agent Influence Power of each agent
    total_budget : float
        The total budget allocated by each firm
    n_iterations : Optional[int], optional
        The number of times to iterate the best response functions, by default
        10

    Returns
    -------
    tuple[np.ndarray (n_agents, 2), np.ndarray (n_iterations, 2)]
        The budget allocated by each firm to each agent and the value of the
        uility of each player at each best response function iteration
    """

    aip = agent_influence_power
    n_agents = aip.shape[0]

    # Initialization
    # allocations = (TOTAL_BUDGET / n_agents) * np.ones((n_agents, 2))
    allocations = np.random.rand(*(n_agents, 2))
    utilities = np.ndarray((n_iterations, 2))

    for i in range(n_iterations):
        # Comupute the best response values
        allocations = best_response(
            initial_opinions, allocations, aip, total_budget
        )

        # Log net_revenue
        utilities[i] = net_revenue(initial_opinions, aip, allocations)

    return allocations, utilities


def gain_of_targeting(allocations: np.ndarray, initial_opinions: np.ndarray, aip: np.ndarray, total_budget: float) -> float:
    """Compute the gain of targeting with respect to firm 1

    Parameters
    ----------
    allocations : np.ndarray (n_agents, 2)
        The budget allocated to each agent by the two firms
    initial_opinions : np.ndarray (n_agents,)
        The opinion of each agent towards firm 1 before any ad campaign
    aip : np.ndarray (n_agents)
        The Agent Influence Power of each agent
    total_budget : float
        The total budget allocated by each firm

    Returns
    -------
    float
        The gain of targeting with respect to firm 1
    """

    n_agents = allocations.shape[0]

    # Uniform allocations
    uniform_allocations = (total_budget / n_agents) * \
        np.ones((n_agents, 2))
    utility_uniform_alloc = net_revenue(
        initial_opinions, aip, uniform_allocations
    )

    # Optimal allocation for firm 1 and uniform allocation for firm 2
    allocations[:, 1] = uniform_allocations[:, 1]
    utility_optimal_alloc = net_revenue(
        initial_opinions, aip, allocations
    )

    return (utility_optimal_alloc[0] - utility_uniform_alloc[0]) \
        / utility_uniform_alloc[0]
