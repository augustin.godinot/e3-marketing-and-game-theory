# Parameter values from the original article
REVENUE_PER_CONSUMER = 1  # \gamma_i
ADVERTISING_EFFICIENCY = 0.1  # \lambda_i
TOTAL_BUDGET = 10  # B_i
INDIV_BUDGET = TOTAL_BUDGET  # b_i
T = 10  # T
LEADER_AIP_VALUES = (5, 10, 100)  # C
N_ITERATIONS = 10  # Number of iterations for the best response policy

MARKERS = ('+', 'o', '^')
COLORS = ('crimson', 'black', 'blue')
