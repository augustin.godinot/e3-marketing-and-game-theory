from pathlib import Path

import click
import numpy as np
from graph_tool import all as gt
from matplotlib import pyplot as pl
from scipy.sparse.linalg import eigs

from constants import N_ITERATIONS, T, LEADER_AIP_VALUES, TOTAL_BUDGET
from game import agent_influence_power, gain_of_targeting, learn_best_response, net_revenue, waterfilling

# OTHER IDEA : generate positive and negative weights to simulate agents hating
# a brand

# OTHER IDEA : highlight Braess paradox: more budget for two firms -> worse
# utility

png_fig = Path('./figures/')
pgf_fig = Path('../report/figures/')
png_fig.mkdir(exist_ok=True, parents=True)
pgf_fig.mkdir(exist_ok=True, parents=True)

pl.rcParams.update({
    "figure.figsize": [.55*6.4, .55*4.8],     # change figure default size
    # "figure.figsize": [6.4, 4.8],     # change figure default size
    "savefig.bbox": "tight",                # image fitted to the figure
    # grid lines for major and minor ticks
    "axes.grid.which": "both",
    "axes.grid": True,                      # always display the grid
    "lines.marker": "+",                    # add a marker to each point
    "lines.linewidth": .7,                  # reduce linewidth to better see the points
    "font.family": "serif",                 # use serif/main font for text elements
    "text.usetex": True,                    # use inline math for ticks
    "pgf.rcfonts": False,                   # don't setup fonts from rc parameters
    "pgf.preamble": "\n".join([
        r"\usepackage{url}",                # load additional packages
        r"\usepackage{unicode-math}",       # unicode math setup
        r"\setmainfont{DejaVu Serif}",      # serif font via preamble
    ])
})


@click.group()
def cli():
    pass


@cli.command()
def article_figure_1():
    """Figure 1 of Marketing resource allocation in duopolies over social networks"""
    n_agents = 100

    n_leader_values = np.linspace(5, n_agents + 1, 20, dtype=int)
    leader_fraction = n_leader_values / n_agents
    gain_of_target = np.zeros_like(n_leader_values, dtype=float)

    fig, ax = pl.subplots()

    for leader_aip in LEADER_AIP_VALUES:
        for i, n_leaders in enumerate(n_leader_values):
            # Agent Influence Power is C for the leaders and 1 for the rest
            aip = np.zeros(n_agents)
            aip[:n_leaders] = leader_aip
            aip[n_leaders:] = 1

            # Uniform initial opinions
            initial_opinions = 0.5 * np.ones(n_agents)

            # Optimal allocations
            allocations, _ = learn_best_response(
                initial_opinions, aip, TOTAL_BUDGET, N_ITERATIONS)

            # Compute gain of targeting
            gain_of_target[i] = gain_of_targeting(
                allocations, initial_opinions, aip, TOTAL_BUDGET
            )

        ax.plot(
            leader_fraction,
            gain_of_target,
            '-+',
            label=f'$C = {leader_aip}$'
        )

    ax.set_xlabel('Fraction of leaders')
    ax.set_ylabel('GoT')
    ax.legend()

    print('SAVING FIGURES ...')
    fig.savefig(pgf_fig.joinpath(
        'gain_of_targeting_2_aip.pgf'
    ))
    fig.savefig(png_fig.joinpath(
        'gain_of_targeting_2_aip.png'
    ), dpi=600)


@cli.command()
def article_figure_2():
    """Figure 2 of Marketing resource allocation in duopolies over social networks"""

    aip = np.array([
        .27, 0.37, 0.98, 0.48, 0.59, 1.58, 0.81, 2.30, 1.09, 1.40, 0.59, 0.81, 0.81, 1.46, 1.46
    ])
    initial_opinions = np.array([
        0.26, 0.76, 0.82, 0.10, 0.18, 0.26, 0.6, 0.52, 0.34, 0.18, 0.21, 0.61, 0.68, 0.47, 0.31
    ])
    n_agents = aip.shape[0]

    allocations, utilities = learn_best_response(
        initial_opinions, aip, TOTAL_BUDGET, N_ITERATIONS
    )

    fig, axes = pl.subplots(2, 1, sharex=True)

    width = 0.35  # the width of the bars
    x = 1 + np.arange(n_agents)

    axes[0].bar(x - width/2, aip, width, label='$\\rho_n$')
    axes[0].bar(x + width/2, initial_opinions, width, label='$x_n(0)$')
    axes[0].set_xticks(x)
    axes[0].legend()
    # axes[0].set_xlabel('agent index $n$')
    axes[0].set_ylabel('parameter value')

    axes[1].bar(x - width/2, allocations[:, 0], width, label='firm 1')
    axes[1].bar(x + width/2, allocations[:, 1], width, label='firm 2')
    axes[1].set_xticks(x)
    axes[1].legend()
    axes[1].set_xlabel('agent index $n$')
    axes[1].set_ylabel('Resource allocation at NE')

    print('SAVING FIGURES ...')
    fig.savefig(pgf_fig.joinpath(
        'allocations_fixed_graph.pgf'
    ))
    fig.savefig(png_fig.joinpath(
        'allocations_fixed_graph.png'
    ), dpi=600)


@cli.command()
def test_waterfilling():
    n_levels = 10
    total_volume = 100

    levels = np.arange(n_levels)
    np.random.shuffle(levels)
    widths = 10 * np.random.rand(n_levels)

    depth, water_level = waterfilling(levels, total_volume, widths)

    x = [0, *np.cumsum(widths)]
    water_height = np.minimum(levels + depth, water_level)

    pl.stairs(levels, x, label='floor level')
    pl.hlines(water_level, 0, max(*x), linestyle='--', label='water level')
    pl.stairs(water_height, x, label='floor level + depth')
    pl.legend()
    pl.show()


@cli.command()
def social_welfare():
    """Figure 1 of Marketing resource allocation in duopolies over social networks"""
    n_agents = 100

    n_leader_values = np.linspace(5, n_agents + 1, 20, dtype=int)
    leader_fraction = n_leader_values / n_agents
    # gain_of_target = np.zeros_like(n_leader_values, dtype=float)
    n_leaders = 10

    total_budget_values = np.logspace(2, 5, 20)
    social_welfare = np.zeros_like(total_budget_values, dtype=float)

    fig, ax = pl.subplots()

    for leader_aip in LEADER_AIP_VALUES:
        # for i, n_leaders in enumerate(n_leader_values):
        for i, total_budget in enumerate(total_budget_values):
            # Agent Influence Power is C for the leaders and 1 for the rest
            aip = np.zeros(n_agents)
            aip[:n_leaders] = leader_aip
            aip[n_leaders:] = 1

            # Uniform initial opinions
            initial_opinions = 0.5 * np.ones(n_agents)

            # Optimal allocations
            allocations, _ = learn_best_response(
                initial_opinions, aip, total_budget, N_ITERATIONS)

            # Compute gain of targeting
            social_welfare[i] = np.sum(net_revenue(
                initial_opinions, aip, allocations))

        # print(social_welfare)
        ax.semilogx(
            total_budget_values,
            social_welfare,
            '-+',
            label=f'$C = {leader_aip}$'
        )
        ax.set_yscale('symlog')

    ax.set_xlabel('Total budget')
    ax.set_ylabel('Social wellfare')
    ax.legend()

    print('SAVING FIGURES ...')
    fig.savefig(pgf_fig.joinpath(
        'social_welfare.pgf'
    ))
    fig.savefig(png_fig.joinpath(
        'social_welfare.png'
    ), dpi=600)


if __name__ == '__main__':
    cli()
