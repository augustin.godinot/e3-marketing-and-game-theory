\documentclass[twocolumn]{color-report}

\usepackage[utf8]{inputenc}		% Encode characters into latex's representation
\usepackage[T1]{fontenc}		% For font encoding
\usepackage[english]{babel} 	% Format text according to language convention
\usepackage{import}				% For advanced imports (relative, no doubles ...)
\usepackage{todonotes}			% For nice TODOs
\usepackage{biblatex}			% For bibliography
\usepackage{csquotes}			% Needed by biblatex polyglossia
\usepackage{graphics}			% To display pictures
\usepackage{bm}					% For bold math caracters
\usepackage{subcaption}			% For mutliple figures in one float
\usepackage{sectsty}            % Tweaks to the sections

\usepackage{grodino}			% custom package with custom math helpers

\usepackage{lettrine}
\usepackage{mathtools}
\usepackage{optidef}
\usepackage{geometry}
\geometry{
    a4paper,
    right=15mm,
    left=15mm,
    top=20mm,
    bottom=20mm
}

\addbibresource{biblio.bib}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STYLES                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\definecolor{black}{RGB}{0, 0, 0}
\definecolor{black-light}{RGB}{31, 30, 33}
\definecolor{primary}{RGB}{156, 0, 60}
\definecolor{accent-1}{RGB}{252, 135, 58}
\definecolor{accent-2}{RGB}{0, 160, 130}
\definecolor{accent-2-light}{RGB}{140, 190, 170}
\definecolor{accent-3}{RGB}{250, 100, 230}
\definecolor{accent-3-light}{RGB}{200, 160, 190}

\mainLogo{logos/CS-round-monochrome.png}
\secondaryLogo{logos/upsaclay.png}

\sectionfont{\centering}
\subsectionfont{\centering}

\captionsetup{
	justification=justified, 
	width=\columnwidth
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% METADATA                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Mandatory fields
\title{Marketing in duopolies}
\author{Charbel Bou-Chaaya \\ Amine Cheikh Mohamed \\ Augustin Godinot}
\date{\today}
\subtitle{E2 - Game Theory}
\reportType{PROJECT REPORT}
\info{}{M2 SAR}
\info{Contact}{%
    Augustin Godinot\hfill \href{mailto:augustin.godinot@student-cs.fr}{augustin.godinot@student-cs.fr} \\
    Charbel Bou Chaaya\hfill \href{mailto:charbel.bou-chaaya@student-cs.fr}{charbel.bou-chaaya@student-cs.fr} \\
    Amine Cheikh Mohamed \hfill \href{mailto:amine.cheikh-mohamed@student-cs.fr}{amine.cheikh-mohamed@student-cs.fr}     
}
\info{Teacher}{%
    Samson Lasaulce \hfill \href{mailto:samson.lasaulce@univ-lorraine.fr}{samson.lasaulce@univ-lorraine.fr}
}


\begin{document}

\maketitle
%\tableofcontents
% \listoffigures
% \listoftables

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUMMARY                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Summary}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Introduction}
\lettrine{G}{ame} Theory is an umbrella term for the science behind
decision-making entities (humans, companies, governments, plants, molecules,
machines, etc.) that studies the deliberate interaction between them. Missing
game theory ideas can be costly in some situations, such as the Braess paradox
(1968), since the availability of a new option is not always advantageous when
the objectives of the agents are nonaligned. The theory's models describe
practical situations from simple daily life human tasks, to global
macroeconomics and wireless networks.

In particular, the proposed paper studies the case of duopolies over social
networks. A duopoly is a market structure in which two firms have exclusive
control over the market. Many real-life examples display this kind of scenario,
in economics, politics, and marketing in general. The novel concept introduced
by this paper links together opinion dynamics portrayed by control theory, and
competition over social networks conceived as a game.

Principally, the paper analyzes a market where two firms attempt to persuade as
many individuals from the network to their viewpoint, using a  certain limited
budget. It is then clear that the core of the problem is that of a game. Many
works have been published for this setting, yet this paper presents a more
general structure for the graph that represents the agents, budget constraints
for the players, and accounts for the instantaneous opinions of the network
nodes. As a consequence, and with these distinctions, the problem in question is
adjusted.
\\

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Problem statement}
We first start by formally defining the variables. The two firms, or players,
are denoted by the index $i \in \{1,2\}$. When $i$ is assigned to a fixed
player, $-i$ refers to the other one. We also assume that the social network is
formed of $N$ agents, designated by the set $\mathcal{V} = \{1,2,\dots,N\}$. The
direct graph $(\mathcal{V, E,} \Omega)$, where $\mathcal{E}$ is the set of edges
and $\Omega$ is the set of labels or weights, illustrates the network. The
opinion of agent $n \in \mathcal{V}$ in favor of firm $1$ is $x_{n;1}(t) =
    x_n(t) \in (0,1)$, where the time $t$ is non-negative. It is also assumed that
when $x_{n;i}(t)$ is closer to $1$, agent $n$ is in favor of firm $1$, and
inversely for firm $2$. We can then write for firm $2$: $x_{n;2}(t) = 1 -
    x_n(t)$. We define $x(t) = \left(x_1(t), x_2(t), \dots, x_N(t)\right)^\top$ the
vector of opinions of the nodes in the network, $x(t) \in \mathcal{X}_0
    =(0,1)^N$.

At the marketing campaign, considered as a time instant, each firm spends a
certain amount of its budget to some nodes of the network. This decision is seen
as an action vector $a_i \in \mathcal{A}_i$ from the action space of firm $i$:
\begin{equation}
    \mathcal{A}_i \coloneqq \left\{ a_i \in [0,b_i]^N \under \sum_{n=1}^N a_{i,n} \leq B_i\right\}
\end{equation}
where $b_i \leq B_i$ are non-negative and depict the maximal contributions of
firm $i$ to one node, and to the whole network respectively. The components of
the action vector $a_i$ are $a_{i.n}$, and they display the investment of firm
$i$ to agent $n$. We presume that the instantaneous campaign takes places at
time $t=0$, and transforms the initial opinions of the agents $x(0) =
    \left(x_{0,1}, x_{0,2}, \dots, x_{0,N}\right)^\top \in \mathcal{X}_0$ according
to a function $\Phi(x_0, a_i, a_{-i}) : \mathcal{X}_0 \times \mathcal{A}_i
    \times \mathcal{A}_{-i} \to \mathcal{X}_0$. This function $\Phi$ is chosen to be
the Bayesian update rule on the opinion $x_n(t)$, seen as the probability of
node $n$ selecting firm $1$. Hence, we have $\Phi(x_0, a_i, a_{-i}) =
    \left(\phi(x_{0,1;i}, a_{i,1}, a_{-i,1}), \dots, \phi(x_{0,N;i}, a_{i,N},
        a_{-i,N})\right)^\top$ where:
\begin{equation}
    \phi(x_{0,n;i}, a_{i,n}, a_{-i,n}) = \frac{x_{0,n;i} + a_{i,n}}{1 + a_{i,n} + a_{-i,n}}
\end{equation}
for all agents $n \in \mathcal{V}$. It is now clear that the supplied budget
$a_{i,n}$ pushes the opinion of agent $n$ towards firm $i$. The function
$\phi(.)$ has three interesting properties: it conserves the symmetry of the
opinions
\begin{equation*}
    \phi(x_{0,n;i}, a_{i,n}, a_{-i,n}) = 1 - \phi(x_{0,n;-i}, a_{-i,n}, a_{i,n})
\end{equation*}
it conserves the same opinion if both marketers do not influence a node
\begin{equation*}
    \lim_{a_{i,n} \to 0, a_{-i,n} \to 0} \phi(x_{0,n;i}, a_{i,n}, a_{-i,n}) = x_{0,n;-i}
\end{equation*}
and when both firms allocate large budget to a certain node, its final opinion
depends solely on the ratio of the contributions
\begin{equation*}
    \lim_{\substack{a_{i,n} \to \infty, a_{-i,n} \to \infty \\ \frac{a_{-i,n}}{a_{i,n}} \to c}}
    \phi(x_{0,n;i}, a_{i,n}, a_{-i,n}) = \frac{1}{1+c}.
\end{equation*}
If $x(0^+)$ is the right-sided limit of $x(0)$, the campaign's influence on the
opinions can be written as:
\begin{equation}
    x_{n;i}(0^+) = \phi(x_{0,n;i}, a_{i,n}, a_{-i,n}).
\end{equation}

Aside from the campaign, the opinion of any consumer is affected by its
neighbors (nodes who have a weighted edge directed towards his node). The
opinion dynamics adopted in the paper is the linear one:
\begin{equation}
    \dot{x}(t) = -\pmb{L}x(t)
\end{equation}
where $\pmb{L} \in \R^{N \times N}$ is the Laplacian matrix of the graph
$(\mathcal{V, E,} \Omega)$. It is defined as the difference between the degree
matrix and the adjacency matrix:
\begin{equation}
    L_{i,j} =
    \begin{cases}
        \text{deg}(i) = \sum_{j=1}^N \Omega_{i,j} & \text{if} \ i = j    \\
        - \Omega_{i,j}                            & \text{if} \ i \neq j
    \end{cases}.
\end{equation}

The final definition is that of the revenue. The paper assumes that profits
collected by firm $i$ after the campaign at any time $T >0$, relies on the
opinions of the consumers towards the firm, while taking into account the clout
of any node on the network. This leverage is expressed by the Agent Influence
Power (AIP). The AIP of agent $n$ is the $n^{th}$ positive element of the row
vector $\rho = 1_N^\top \exp(-\pmb{L} T)$. The gain acquired by firm $i$ is then:
\begin{equation*}
    u_i(x_0, a_i, a_{-i}) \coloneqq \gamma_i \rho x_i(0^+) - \lambda_i 1_N^\top a_i
\end{equation*}
where $\gamma_i$ is the non-negative revenue per consumer, and $\lambda_i$ is a
non-negative factor that describes the efficiency of advertising, both fixed for
firm $i$.

At this point, the problem's setting allows us to express it accordingly as a
strategic form game in this fashion:
\begin{equation}
    \mathcal{G} = \left(\{1,2\}, \{\mathcal{A}_i\}_{i=1,2}, \{u_i\}_{i=1,2}\right).
\end{equation}
This kind of games is characterized by a triplet, where the first term is the
set of players, the second is the strategy sets for the players, and the third
term is the set of objective functions. Using direct game theory, and after
choosing the representation, we now focus on the solution concept. The one
disclosed in the paper is the pure Nash Equilibrium (NE). Actions taken by the
players attain this equilibrium when no player has an advantage to deviate his
decision. Formally, for a given initial $x_0$, a strategy profile $(a^\star_1,
    a^\star_2) \in \mathcal{A}_1 \times \mathcal{A}_2$ is a pure NE for
$\mathcal{G}$ if $\forall i \in \{1, 2\}, \forall a_i \in \mathcal{A}_i$,
\begin{equation}
    u_i(x_0, a^\star_i, a^\star_{-i}) \geq u_i(x_0, a_i, a^\star_{-i}).
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Game-theoretic analysis}
After picking the solution approach, we start analyzing its existence,
uniqueness and efficiency.

The authors choose to apply Rosen's theorem (1965) to prove that this game has a
pure NE. First, in the finite dimensional space $\R^N_{\geq 0}$, the closed sets
$\mathcal{A}_i$ are compact since are subsets of the compact hypercube having
each edges of length $B_i$. It is also convex, since $\forall a_i, a_i^\prime
    \in \mathcal{A}_i, \forall \lambda \in [0, 1], \sum_{n=1}^N \lambda a_{i,n} +
    (1-\lambda) a^\prime_{i,n} = \lambda \sum_{n=1}^N a_{i,n} + (1 - \lambda)
    \sum_{n=1}^N a^\prime_{i,n} \leq B_i$, meaning $\lambda a_i + (1-\lambda)
    a_i^\prime \in \mathcal{A}_i$. Then, we observe that $u_i$ is jointly continuous
in $\mathcal{A}_i$ and twice differentiable. Now we prove it is also concave by
evaluating:
\begin{equation*}
    \frac{\partial^2 u_i}{\partial a_{i,n} \partial a_{i,m}} =
    \begin{cases}
        -\frac{2 \gamma_i \rho_n (1-x_{0,n;i}+a_{-i,n})}{(1+a_{i,n}+a_{-i,n})^3} & \text{if} \ m=n      \\
        0                                                                        & \text{if} \ m \neq n
    \end{cases}
\end{equation*}
Therefore the Hessian matrix associated to the utility function is diagonal with
negative elements. Thus, its eigenvalues are also negative, and the matrix
itself is negative definite, which means that $u_i$ is concave in
$\mathcal{A}_i$. The final step is to prove that the weighted sum of utilities
satisfies the Diagonally Strict Concavity (DSC) condition. We first write the
pseudo-gradient of the weighted sum for some $r_1, r_2 > 0$, and strategy $a \in
    \mathcal{A}_1 \times \mathcal{A}_2$ as:
\begin{equation*}
    g(a, r) = \left(r_1 \nabla_{a_1} u_1(a), r_2 \nabla_{a_2} u_2(a)\right)^\top
\end{equation*}
For the same initial opinions, the DSC condition is to find weights $r > 0$,
such that $\forall a, a^\prime \in \mathcal{A}_1 \times \mathcal{A}_2, a \neq
    a^\prime$
\begin{equation*}
    (a - a^\prime)^\top (g(a,r) - g(a^\prime,r)) < 0
\end{equation*}
A sufficient condition for DSC, in this case, is that the jacobian $G(a,r)$ of
$g(a,r)$ w.r.t $a \in \mathcal{A}_1 \times \mathcal{A}_2$ satisfies:
\begin{equation*}
    G(a,r) + G(a,r)^\top \prec 0
\end{equation*}
We start by fixing $r = \left(\frac{1}{\gamma_1}, \frac{1}{\gamma_2}\right)$.
The diagonal terms of $G(a,r)$ are $\frac{1}{\gamma_i} \frac{\partial^2
        u_i}{\partial^2 a_{i,n}} < 0$. The off-diagonal terms are $\frac{1}{\gamma_i}
    \frac{\partial^2 u_i}{\partial a_{i,n} \partial a_{j,m}}$. They are zero when $m
    \neq n$, and when $m = n$ we have:
\begin{align*}
     & \frac{1}{\gamma_i} \frac{\partial^2 u_i}{\partial a_{i,n} \partial a_{-i,n}}
    = \rho_n \frac{-1 -a_{-i,n}+2x_{0,n;i}+a_{i,n}}{(1+a_{i,n}+a_{-i,n})^3}           \\
     & =-\rho_n \frac{-1 -a_{-i,n}+2x_{0,n;-i}+a_{i,n}}{(1+a_{i,n}+a_{-i,n})^3}       \\
     & =-\frac{1}{\gamma_i} \frac{\partial^2 u_i}{\partial a_{-i,n} \partial a_{i,n}}
\end{align*}
Thus, the terms at $(n, N+n)$ and $(N+n, n)$ are opposite. Finally, $G(a,r) +
    G(a,r)^\top$ is a diagonal matrix with negative terms, meaning it is negative
definite since all its eigenvalues are negative.

Since all necessary conditions to apply Rosen's theorem are valid, then
$\mathcal{G}$ has a unique pure NE. To find the actions taken by the players at
equilibrium, the authors propose to use the best response dynamics, that is when
each player looks to maximize his utility while seeing how the other one is
acting. This course of actions leads to the NE, and the actions taken by firm
$i \in \{1,2\}$ towards agent $n \in \mathcal{V}$ satisfy:
\begin{equation}
    \dot{a}_{i,n} = \beta_{i,n}(x_{0;i}, a_{-i}) - a_{i,n}
\end{equation}
where $\beta_{i}(x_{0;i}, a_{-i}) \coloneqq \argmax_{a_i} \{u_i(x_{0;i}, a_i,
    a_{-i})\}$ can be written in this form because the objective functions are
proven to be strictly concave. At NE, we have $a^\star_i = \beta_i(x_{0,1},
    a^\star_{-i})$ and this point $(a^\star_1, a^\star_2)$ is unique in
$\mathcal{A}_1 \times \mathcal{A}_2$. To find this NE, we must evaluate the best
response functions. In fact, these functions are the solutions to the following
optimizations problems for $i \in \{1,2\}$ written under standard form:
\begin{mini*}|l|
    {a_i}{-u_i(x_0, a_i, a_{-i})}{}{}
    \addConstraint{-a_{i,n} \leq 0}
    \addConstraint{a_{i,n} - b_i \leq 0 \quad \forall n \in \mathcal{V}}
    \addConstraint{\sum_{n=1}^N a_{i,n} - B_i \leq 0 \quad \forall n \in \mathcal{V}}
\end{mini*}
This problem has a solution since the optimization space $\mathcal{A}_i$ is
compact and $u_i$ is continuous. Also, all inequality constraints are defined
from affine functions, and hence the LCQ condition is valid. This means the
problem is strongly dual, and the KKT conditions are sufficient for optimality:
$$\begin{cases*}
        \nabla_{a_i} \mathcal{L} =
        \nabla_{a_i} u_i(x_0, a_i^\star, a_{-i}^\star) - \mu_0 \nabla_{a_i} \sum_{n=1}^N a^\star_{i,n} +     \\
        \sum_{n=1}^N (\mu_n - \mu_{-n}) \nabla_{a_i} a_{i,n}^\star = 0                                       \\
        -a^\star_{i,n} \leq 0 \quad a^\star_{i,n} - b_i \leq 0 \quad \sum_{n=1}^N a^\star_{i,n} - B_i \leq 0 \\
        \mu_n \geq 0 \quad \forall n \in \{-N,\dots,N\}                                                      \\
        \mu_0 (\sum_{n=1}^N a^\star_{i,n} - B_i) = 0                                                         \\
        \mu_n (a^\star_{i,n} - b_i) = 0, \mu_{-n} a^\star_{i,n} = 0 \quad \forall n \in \{1,\dots,N\}
    \end{cases*}$$
where $\mu_n$ are the Lagrange multipliers $n \in \{-N,\dots,N\}$
We have $\frac{\partial u_i(x_0,a_i,a_{-i})}{\partial a_{i,n}} = \frac{\gamma_i
    d_{i,n}}{(1+a_{i,n}+a_{-i,n})^2} - \lambda_i$ where $d_{i,n} \coloneqq \gamma_i
    \rho_n (x_{0,n;-i}+a_{-i,n})$ is a constant. The first condition is now written:
\begin{equation*}
    \frac{d_{i,n}}{(1+a_{i,n}+a_{-i,n})^2} = \lambda_i + \mu_0 + \mu_n - \mu_{-n} \ \forall n \in \mathcal{V}
\end{equation*}
The Lagrange multipliers for the agents who already have a budget of $0$ or
$b_i$ can be disregarded since the constraint is satisfied. Then we have:
\begin{equation*}
    \beta_{i,n}(x_{0;i},a_{-i}) =
    \begin{cases}
        0                                                       & \text{if} \ n \in \mathcal{W}_0 \\
        b_i                                                     & \text{if} \ n \in \mathcal{W}_1 \\
        \sqrt{\frac{d_{i,n}}{\mu_0 + \lambda_i}} - 1 - a_{-i,n} & \text{if} \ n \in \mathcal{W}_2
    \end{cases}
\end{equation*}
where $\mathcal{W}_0, \mathcal{W}_1$ are subsets of $\mathcal{V}$ and
$\mathcal{W}_2 = \mathcal{V} \setminus \mathcal{W}_0 \setminus \mathcal{W}_1$.
The final condition allows us to calculate $\mu_0$, and finally we must make
sure that $0 \leq \beta_{i,n}(x_{0;i},a_{-i}) \leq b_i$ when $n \in
    \mathcal{W}_2$. We can then write:
\begin{align}\label{eq:best_response}
    \beta_{i,n}(x_{0;i},a_{-i}) = \quad \nonumber \\
    \max \left(0, \min \left( b_i, \sqrt{\frac{d_{i,n}}{\mu_{0;i} + \lambda_i}} - 1 - a_{-i,n} \right) \right)
\end{align}
where $\mu_{0;i} \geq 0$ satisfies the total budget complimentary slackness
condition.

After dynamically outlining the NE, the authors also provide an insight of the
marketing allocation at the equilibrium and the problem's various parameters,
like the initial opinions and the AIP. We notice that from the characterization
we have, that if the budget $a^\star_{i,n}$ is nor $0$ and nor its maximum
$b_i$, then
\begin{equation}
    a^\star_{2,n} = \frac{k_2}{k_1} (x_{0,n} + a^\star_{1,n}) + x_{0,n} - 1
\end{equation}
for all nodes $n \in \mathcal{V}$, where $k_i =
    \frac{\gamma_i}{\mu_{0;i}+\lambda_i}$. Injecting this equation in the NE
equation yields for $i \in \{1,2\}$:
\begin{equation}
    a^\star_{i,n} = \left(\frac{k_i}{k_i + k_{-i}}\right)^2 k_{-i} \rho_n - x_{0,n;i}
\end{equation}
This means that the allocation given to an agent is inversely proportional to
his initial opinion, and increases with his AIP.

If the budget of any node $n \in \mathcal{V}$ at the NE $(a^\star_{1,n},
    a^\star_{2,n})$ is $0$, then it takes the form $(t,0)$ (or $(0,t)$) if $\exists
    \ t \in [0,b_1]$ (or $[0,b_2]$) such that this action vector satisfies the NE.
Otherwise, if the node takes its maximum amount, then the NE has the form
$(t,b_2)$ (or $(b_1,t)$) if $\exists \ t \in [0,b_1]$ (or $[0,b_2]$) such that
this vector attains the NE.
\\

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Numerical performance analysis}
In this final section, the authors present two examples that illustrate the
importance of knowing the target audience before launching the campaign.

In the first case, we compare the objectives of a marketer who uses a uniform
budget allocation (UBA) with $a_i^{UBA} \coloneqq B_i / N$, to that of a more
clever marketer who knows the structure of the market and distributes his budget
while maximizing his utility. The first kind of firms can be seen as a classical
media outlet that does not target a specific audience, but rather a general one.

To emphasize on the relevance of the network-aware marketing, the AIP of an
agent $\rho_n$ is either fixed to a constant $C$ and this person is considered
as a 'leader', or it is set to $1$ for the rest of the nodes. The Gain of
Targeting (GoT), defined as:
\begin{equation*}
    \text{GoT} \coloneqq \frac{u_1(x_0, \beta_1(a_2^{UBA}),a_2^{UBA}) - u_1(x_0, a_1^{UBA}, a_2^{UBA})}{u_1(x_0, a_1^{UBA}, a_2^{UBA})}
\end{equation*}
measures the importance of steering the budget in an effective manner. The
reported graph of the GoT as function of the percentage of leaders and for
different values of the AIP shows that this gain increases with $C$, where
assigning a larger amount to the leaders is improves the utility. It decreases
when the number of leaders is major, where both types of allocation are the
same.

In the second example, the authors study a given network, to demonstrate the
impact of initial opinions on the NE resource distribution. We start by randomly
generating an initial opinion vector $x(0)$, and calculating the AIP $\rho$ from
the weights of the graph. Now, we calculate the equilibrium allocations
$(a^\star_1, a^\star_2)$. First, we notice that agents with higher AIP receive
higher shares of the budget from both firms. Also, we can notice that each firm
distributes more of its budget to nodes that have an unfavorable opinion about
it, in order to change it.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% IMPLEMENTATION                                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Implementation}

In this section, we describe the implementation of the figures 1 and 2 from
\cite{varmaDynamics2018}. The code is sent along this report or can be found on
\href{https://gitlab-student.centralesupelec.fr/augustin.godinot/e3-marketing-and-game-theory}{GitLab}.
Along the comments in the code, you will find installation/running instruction
in the file \texttt{readme.md}.

The main technicality in recreating the figures lies in implementing the best
response functions of the two firms (\autoref{eq:best_response}). It requires
performing optimal budget allocation using a waterfilling algorithm.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Waterfilling algorithm}
In this section we describe a waterfilling algorithm that will be used to
compute the best responses. Discarding the previous notations, the waterfilling
allocation problem we consider is finding $\mu$ such that
\begin{equation}
    \forall n \in \intrvl{1}{N}, x_n = a_n (\mu - b_n)^+ \text{ and } \sum_{n = 1}^N x_n < B
\end{equation}
$\mu$ is called the water level, $b_n$ the floor level, $a_n$ the floor width
and $B$ the total volume of water. $x^+ = \max(0, x)$. The idea of waterfilling is to sort the
values by increasing floor level $b_n$ and then "flood" the levels one by one
until we have "not water left". The algorithm is described in
\autoref{algo:waterfilling} and was inspired by \cite{heWaterFilling2013}.

\begin{algorithm*}
    \SetAlgoLined
    \DontPrintSemicolon
    \KwIn{%
        $a_n$, $b_n$ and $B$, sorted by increasing $b_n$
    }
    \KwResult{The water level $\mu$}

    $i \leftarrow 0$

    \While{$n \leq N - 1$}{%
        $r \leftarrow B - \sum_{i=1}^n a_i (\mu - b_i)$ \tcp*[r]{remaining water}
        $\Delta \leftarrow b_{i+1} - b_i$ \tcp*[r]{level difference}

        \eIf(\tcp*[r]{Check if we have enough water left}){$r < \Delta \sum_{i=1}^n a_n$}{%
            $\Delta_a \leftarrow r / \sum_{i=1}^n a_n$ \tcp*[r]{achievable water difference}
            $\mu \leftarrow \mu + \Delta_a$
        }{%
            $\mu \leftarrow \Delta$
        }

        \Return $\mu$ \tcp*[r]{return the water level}
    }

    \caption{Geometric waterfilling}
    \label{algo:waterfilling}
\end{algorithm*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Best response functions}
As the algorithm seen in the previous section can only account for the total
budget $B_i$, we consider for the simulations (as in \cite{varmaDynamics2018})
that $b_i = B_i$.

Using \autoref{eq:best_response}, we have
\begin{equation}
    \beta_{i, n}(x_{0;i}, a_{-i}) = \underbrace{\sqrt{\gamma_i d_{i, n}}}_{\text{width}} \left(
    \underbrace{\frac{1}{\sqrt{\mu_{0;i} + \lambda_i}}}_{\text{water level}}
    - \underbrace{\frac{1 + a_{-i;n}}{\sqrt{\gamma_i d_{i,n}}}}_{\text{floor level}}
    \right)^+
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXPERIMENTS                                                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Experiments}
In this section, we describe the experiments we tried with this model. Unless
stated otherwise, the parameters values are that of the
\autoref{tab:simulation_parameters}.

\begin{table}
    \centering
    \begin{tabular}{c c c c c}
        $T$  & $B_i$, $b_i$ & $\gamma_i$ & $\lambda_i$ \\
        \hline
        $10$ & $10$         & $1$        & $0.1$
    \end{tabular}
    \caption{Parameters of the simulations}
    \label{tab:simulation_parameters}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Gain of targeting and resource allocation}
In \autoref{fig:gain_of_targeting_2_aip} and
\autoref{fig:allocations_fixed_graph} are plotted the same experiments as in
\cite{varmaDynamics2018}.

\begin{figure}
    \input{figures/gain_of_targeting_2_aip.pgf}
    \caption{Gain by implementing the best response strategy over
        the uniform budget allocation strategy based on the
        fraction of leaders.}
    \label{fig:gain_of_targeting_2_aip}
\end{figure}

\begin{figure}
    \input{figures/allocations_fixed_graph.pgf}
    \caption{The sub-figure on top shows the AIP $\rho_n$ and initial
        opinion $x_n(0)$ for Agent $n,\; n \in \{1, ..., N\}$. Corresponding to
        this initial configuration, the sub-figure on the bottom shows
        the resource allocation strategies at the NE}
    \label{fig:allocations_fixed_graph}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Effect of the total budget}
In game theory, the Braess paradox \cite{Braess1968} describes situations where
increasing the number of actions available to the players leads to a worse
situation. In our case, the number of actions is not countable but if we think
in terms of volume of the action space, it is determined by the total budget.

To highlight this effect, we consider $N = 100$ agents, composed of $10$ leaders
with AIP $C \in (5, 10, 100)$ and $90$ followers with AIP $1$. The initial
opinions are symmetric $x_n(0) = \frac{1}{2}$. We then compute the Nash
Equilibrium for different values of total budget and plot the results in
\autoref{fig:social_welfare}. As expected, after a given budget (between $10^3$
and $10^4$), increasing the budget drastically decreases the social welfare.

\begin{figure}
    \input{figures/social_welfare.pgf}
    \caption{%
        Social welfare of the game at equilibrium for different values of total
        budget. As the budget of the firms increases, the social welfare
        decreases significantly.
    }
    \label{fig:social_welfare}
\end{figure}

\printbibliography

\end{document}